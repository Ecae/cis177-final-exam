__author__ = 'jhoeflich2017'
from tkinter import *
from math import *

#Quick explaination, I tried to do this in tkinter but it would have taken too much time
#so I had to abandon the idea halfway through, but I still used the oval creation from tkinter
#(aka, x1,x2,y1,y2) because it would have taken too much time to start over.
#In my defense you can still technically set the center if you think about it.

class Circle2D:
    def __init__(self, x1, y1, x2, y2):
        self.x1 = float(x1)
        self.y1 = float(y1)
        self.x2 = float(x2)
        self.y2 = float(y2)
        self.radius = (sqrt((x2-x1)**2+(y2-y1)**2))/2
    #There are functions to get each of the corner values (x1,y1,x2,y2) as well as the center of the circle
    #which is just the midpoint between the two x's and the two y's
    def getCenterX(self):
        return (self.x1 + self.x2)/2
    def getCenterY(self):
        return (self.y1 + self.y2)/2
    def get_x1(self):
        return self.x1
    def get_y1(self):
        return self.y1
    def get_x2(self):
        return self.x2
    def get_y2(self):
        return self.y2
    def get_radius(self):
        return self.radius
    def set_x1(self,x1):
        self.x1 = float(x1)
    def set_y1(self,y1):
        self.y1 = float(y1)
    def set_x2(self,x2):
        self.x2 = float(x2)
    def set_y2(self,y2):
        self.y2 = float(y2)
    def set_radius(self,radius):
        self.radius = radius
    def getArea(self,radius):
        area = (radius**2)*pi
        print('The area is', area)
    def getPerimeter(self,radius):
        perimeter = (radius*2)*pi
        print('Ther perimeter is', perimeter)
    def containsPoint(self,x, y):
        #I cant get this to work with booleans
        if self.x1 <= x <= self.x2 and self.y1 <= y <= self.y2:
            print('The point is in the circle')
        else:
            print('Not in the circle')
    def contains(self,radius1, radius2, x, y):
        r1 = (radius1**2)*pi
        r2 = (radius2**2)*pi
        #if r1 >= r2:
    #def overlaps(self):
        #code = working
